## Working with Spring boot and GraphQL

When creating applications, REST is an often used technology to transfer data through APIs. While REST is commonly adopted, there are some issues with it. In this tutorial, I’ll show you how GraphQL compares to REST and how to use GraphQL with Spring boot.

### Issues with REST
When using REST, each resource usually has an endpoint. For example, when writing a blog application, you may have an ```/article``` endpoint to fetch the information of the article, a ```/profile``` endpoint to fetch the information of the user profile and perhaps even a ```/comments``` endpoint to retrieve the comments for a given article.

The issue is that, when you’re retrieving the articles to show a list of articles, you need different data compared to when you’re retrieving the full blown article with all of its details. This leads to a lot of overfetching, considering that you’ll fetch too much data to build the article overview page if you use the same endpoints to handle both the article overview and the article detail page.

![image info](./assets/rest-overfetch.png "Traffic flow when using REST APIs")

A possible solution is to provide less information when someone calls ```/api/article``` compared to when someone is calling ```/api/article/123```. This might solve the issue, but what if you want to write your application for both mobile devices and web browsers? Your mobile application might need even less data, so you could still be overfetching. A possible solution to this problem is to have multiple endpoints, for example ```/api/article/mobile``` and ```/api/article/desktop```. This however, leads to a much higher coupling between the view and the REST APIs.

### GraphQL
But what if, you have something like REST, but you could pick the data you need? This is, simplified, what GraphQL provides. This means, that GraphQL is not just something that lives on the frontend or something that lives on the backend, it’s both. GraphQL basically has two major concepts, which are:

* **Types:** Types are definitions or interfaces of you API. It contains the various types of your API and how they’re linked and what operations are possible.
* **Resolvers:** Resolvers contain the mappings from the types to their actual implementation.

There are a few GraphQL libraries to start with though. Since Facebook came up with the concept, they also have their own library called _Relay_. The group behind _Meteor_ also created their own library, called _Apollo_. Additionally to those two, there are various implementations in most programming languages, and Java is no different with graphql-java.

In this tutorial I’ll talk about how to use GraphQL with Spring boot.

### GraphQL + Spring boot

#### Setting up project
To set up your Spring boot project, you can use the Spring Initializr. In my case, I want to add JPA and HSQLDB for my persistence layer, Lombok to be able to write classes in fewer lines of code and the Web dependency to be able to use my application as a web application.

Additionally to that, I’m also going to add the graphql-spring-boot-starter and graphiql-spring-boot-starter dependencies to my project. The first one provides the GraphQL endpoint, while the second one provides a graphical interface to debug your GraphQL queries.

```
<dependency>
    <groupId>com.graphql-java</groupId>
    <artifactId>graphql-spring-boot-starter</artifactId>
    <version>${graphql-java.version}</version>
</dependency>
<dependency>
    <groupId>com.graphql-java</groupId>
    <artifactId>graphiql-spring-boot-starter</artifactId>
    <version>${graphql-java.version}</version>
</dependency>
```
I configured ${graphql-java.version} to be 5.0.2.


#### Writing your types
In this article, I’ll be using a “blog application” as an example. There will be three types, articles, comments and profiles. The link between them is shown below:

![image info](./assets/graphql-type-relation.png "Relation between the types")

To start off, create a file called ```types.graphqls``` in ```src/main/resources```. The first type we have to define is the root/global type called Query:

```
type Query {

}
```

In this type, we can define the various “operations” that will be possible. In this example, I’ll add four possible operations:

* An operation to retrieve a list of all articles.
* Another operation to retrieve a list of all profiles.
* An operation to retrieve a list of all comments.
* And finally, an operation to retrieve an article by its ID.

To do this, we modify the Query type to be like this:
```
type Query {
    articles: [Article]
    profiles: [Profile]
    comments: [Comment]
    article(id: Int!): Article
}
```
As you can see, we use the ```\[Article]``` syntax to define an array, and the ```(id: Int!)``` syntax to explain that this operation requires a parameter called id. The exclamination mark (Int!) means that the parameter is required.

The next step is to define the other types, being Article, Profile and Comment:

```
type Article {
    id: Int!
    title: String!
    text: String!
    author: Profile!
    comments: [Comment]
}

type Profile {
    id: Int!
    username: String!
    bio: String
}

type Comment {
    id: Int!
    text: String!
    author: Profile!
}
```
The way this works is similar to the Query type. Fields that are required have an exclamination mark behind them, while arrays are surrounded by square brackets.

#### Writing the actual models
The types we just wrote, are a representation of our entities. This means that we also need an actual implementation of these types. In this case I’ll be using JPA to be able to persist my entities. For example, the Article class will look like this:

```
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String text;
    private Long authorId;
}
```
The Profile entity will contain the following data:

```
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String bio;
}
```
And finally, the Comment will look like this:

```
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private Long articleId;
    private Long authorId;
}
```

#### Writing the persistence layer

Before we start with the next part of GraphQL (the resolvers), we have to write the repositories that will be used to fetch data from the database. I’ll be using Spring Data JPA here to write my repository interfaces like this:

```
public interface ArticleRepository extends JpaRepository<Article, Long> {
}

public interface ProfileRepository extends JpaRepository<Profile, Long> {
}

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List findByArticleId(Long articleId);
}
```

Most repositories here are quite simple, and since we’ll probably only use the default methods (findOne(), findAll()), you don’t have to write any methods. The only exception is the CommentRepository, since we want to be able to fetch all comments for a given Article.

#### Getting started with a resolver

Resolvers allow you to map certain field in your types to specific actions. When using graphql-java, there are two types of resolvers:

**_GraphQLQueryResolver_** is the main resolver, used to resolve any properties or operations within the Query type
**_GraphQLResolver_** is the resolver for any other types. It uses generics to be able to detect which type it’s about. An example would be _GraphQLResolver<Article>_.
To resolve a specific property, the library will look for any methods matching the property name. For example, the articles property within Query will be matched to a method called articles(), getArticles() or isArticles().

To get started, you need to implement GraphQLQueryResolver, for example:

```
@Component
@AllArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {
    private ArticleRepository articleRepository;
    private CommentRepository commentRepository;
    private ProfileRepository profileRepository;

    public List<Article> getArticles() {
        return articleRepository.findAll();
    }

    public List<Comment> getComments() {
        return commentRepository.findAll();
    }

    public List<Profile> getProfiles() {
        return profileRepository.findAll();
    }

    public Article getArticle(Long id) {
        return articleRepository.findOne(id);
    }
}
```
As you can see, these method names match the property names of the Query type.

The next step is to write resolvers for every other type. This is necessary, because we want to link the comments and the author to the article, but also the author to the comment itself. The profile doesn’t need any custom resolvers, since all fields of Profile are basic fields.

First let’s create an implementation of GraphQLResolver<Article>:

```
@Component
@AllArgsConstructor
public class ArticleResolver implements GraphQLResolver<Article> {
    private CommentRepository commentRepository;
    private ProfileRepository profileRepository;

    public Profile getAuthor(Article article) {
        return profileRepository.findOne(article.getAuthorId());
    }

    public List<Comment> getComments(Article article) {
        return commentRepository.findByArticleId(article.getId());
    }
}
```
As you can see, the setup is quite similar to the resolver we wrote before. The only exception is that we now have access to the parent object, which is the Article that is being resolved.

Similar to this, we can create our CommentResolver:

```
@Component
@AllArgsConstructor
public class CommentResolver implements GraphQLResolver<Comment> {
    private ProfileRepository profileRepository;

    public Profile getAuthor(Comment comment) {
        return profileRepository.findOne(comment.getAuthorId());
    }
}
```

#### Testing it out
Now that we’ve implemented our resolvers, it’s time to test it out. I made a simple CommandLineRunner to insert some mock data:

```
@Component
@AllArgsConstructor
public class DataProvider implements CommandLineRunner {
    private CommentRepository commentRepository;
    private ArticleRepository articleRepository;
    private ProfileRepository profileRepository;


    @Override
    @Transactional
    public void run(String... strings) {
        Profile author = profileRepository.save(new Profile(null, "g00glen00b", "The author of this blog"));
        Profile admin = profileRepository.save(new Profile(null, "admin", "The administrator of this blog"));
        Article article1 = articleRepository.save(new Article(null, "Hello wold", "This is a hello world", author.getId()));
        Article article2 = articleRepository.save(new Article(null, "Foo", "Bar", admin.getId()));
        commentRepository.save(new Comment(null, "Do you like this article?", article1.getId(), author.getId()));
        commentRepository.save(new Comment(null, "This is a great article", article1.getId(), admin.getId()));
        commentRepository.save(new Comment(null, "This is a comment", article2.getId(), admin.getId()));
    }
}
```

And now, if we run the application and go to http://localhost:8080/graphiql, we should be able to see our GraphQL tester.

![image info](./assets/Screenshot-2018-02-03-17.19.42.png "Example of GraphiQL")

Now, let’s say that we want to write a GraphQL query to show a list of articles, but we don’t need the comments, nor the bio of the author. If we would like to do so, we could write the following query:

```
query AllArticles {
  articles {
    id
    title
    author {
      id
      username
    }
  }
}
```

As you can see, we’re only retrieving the id and the title of the article, and the username of the author. If you would debug you code, you would see that it will never retrieve the comments in this case.

![image info](./assets/Screenshot-2018-02-03-19.22.46.png "GraphiQL interface when retrieving all articles")

The next interesting query would be to retrieve all information of the current article, including the bio of the author and the comments from the other users. This query would use the article property of Query, but that means we need to pass the article ID somehow. To do this, we need to write the following query:
```
query Article($articleId: Int!) {
  article(id: $articleId) {
    id
    title
    author {
      id
      username
      bio
    }
    comments {
      id
      text
      author {
        id
        username
      }
    }
  }
}
```
As you can see, we’re defining a parameter called ```articleId``` and we pas this ID to the article endpoint as the parameter id. The resolver will then pick this up and invoke the correct method.

To be able to test this in GraphiQL, you need to select Query variables at the bottom, and enter your parameters as a JSON:

```
{"articleId": 1}
```

Normally, GraphiQL will already initialize an empty object containing the "articleId" key as soon as you start to enter the ID.

![image info](./assets/Screenshot-2018-02-03-19.29.20.png "GraphiQL interface using query variables")


### Writing GraphQL mutations with Spring boot

Writing your mutation type
Just like last time, when we created our own Query type, it’s now time to create a type called Mutation:
```
type Mutation {
    createArticle(input: CreateArticleInput!): Article!
    updateArticle(input: UpdateArticleInput!): Article!
    deleteArticle(id: Int!): Int!
    createProfile(input: CreateProfileInput!): Profile!
    updateProfile(input: UpdateProfileInput!): Profile!
    deleteProfile(id: Int!): Int!
    createComment(input: CreateCommentInput!): Comment!
    deleteComment(id: Int!): Int!
}
```
In this case, I’m going to allow both creating, updating and deleting on articles and profiles, but only allowing to create and delete comments. People who wish to change their comment will have to remove it to post a new one.

As you can see in the example above, we’re using the Article, Profile and Comment types from before, but we’re also returning an Int for every delete action (deleteArticle(), deleteProfile() and deleteComment()). The reasoning behind this is that GraphQL doesn’t support void types as far as I know, so let’s return the amount of records that are being deleted at that time (usually either zero or one, depending on if the ID exists or not).

#### Defining custom inputs
We’ve seen how to use predefined inputs like Int before, but for our mutations, we’d like to use custom inputs, like CreateArticleInput. This allows us to pass multiple parameters, such as the title and the text of the article.

Creating a custom input is similar to creating a type:
```
input CreateArticleInput {
    title: String!
    text: String!
    authorId: Int!
}
```
The most important difference is that we start by writing input rather than type.

Just like that, we can create the other custom inputs:

```
input UpdateArticleInput {
    id: Int!
    title: String!
    text: String!
}

input CreateProfileInput {
    username: String!
    bio: String
}

input UpdateProfileInput {
    id: Int!
    bio: String
}

input CreateCommentInput {
    text: String!
    authorId: Int!
    articleId: Int!
}
```
In my example, I won’t allow the user to update the author ID of an article, nor the username of a profile, so I left those away in UpdateArticleInput and UpdateProfileInput respectively. Another difference between the create and update inputs is that we need to provide the ID of the article, comment or profile we wish to update, so be aware of that as well.

#### Mapping the types to Java classes
The next step is to create proper Java implementations of our custom types. In this case, we need to create classes for the CreateArticleInput, UpdateArticleInput, CreateProfileInput, UpdateProfileInput and CreateCommentInput types:

```
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateArticleInput {
    private String title;
    private String text;
    private Long authorId;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateArticleInput {
    private Long id;
    private String title;
    private String text;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateProfileInput {
    private String username;
    private String bio;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProfileInput {
    private Long id;
    private String bio;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateCommentInput {
    private String text;
    private Long authorId;
    private Long articleId;
}
```

#### Writing your resolver
As explained in my previous article, GraphQL consists out of two main components, one being types and another one being resolvers. Last time, we wrote our main resolver by implementing the GraphQLQueryResolver interface. This time, we need to do a similar thing, but rather than implementing GraphQLQueryResolver, we need to implement the GraphQLMutationResolver interface.

```
@Component
@AllArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {

}
```

Now, all we have to do is to implement our resolvers by creating methods using the same name as the properties in the Mutation type. For example:

```
@Component
@AllArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {
    private ArticleRepository articleRepository;
    private CommentRepository commentRepository;
    private ProfileRepository profileRepository;

    @Transactional
    public Article createArticle(CreateArticleInput input) {
        return articleRepository.saveAndFlush(new Article(null, input.getTitle(), input.getText(), input.getAuthorId()));
    }
}
```
Now we can repeat this for all other methods as well:

```
@Transactional
public Article updateArticle(UpdateArticleInput input) {
    Article article = articleRepository.findById(input.getId()).orElseThrow(ArticleNotFoundException::new);
    article.setText(input.getText());
    article.setTitle(input.getTitle());
    return article;
}

@Transactional
public int deleteArticle(Long id) {
   return articleRepository.deleteById(id);
}

@Transactional
public Profile createProfile(CreateProfileInput input) {
    return profileRepository.saveAndFlush(new Profile(null, input.getUsername(), input.getBio()));
}

@Transactional
public Profile updateProfile(UpdateProfileInput input) {
    Profile profile = profileRepository.findById(input.getId()).orElseThrow(ProfileNotFoundException::new);
    profile.setBio(input.getBio());
    return profile;
}

@Transactional
public int deleteProfile(Long id) {
    return profileRepository.deleteById(id);
}

@Transactional
public Comment createComment(CreateCommentInput input) {
    return commentRepository.saveAndFlush(new Comment(null, input.getText(), input.getArticleId(), input.getAuthorId()));
}

@Transactional
public int deleteComment(Long id) {
    return commentRepository.deleteById(id);
}
```
To be able to do this, I also created a custom ArticleNotFoundException and ProfileNotFoundException:

```
public class ArticleNotFoundException extends RuntimeException {
}

public class ProfileNotFoundException extends RuntimeException {
}
```
Additionally, I also added a few methods to my repositories:

```
public interface ArticleRepository extends JpaRepository<Article, Long> {
    Optional<Article> findById(Long id);
    int deleteById(Long id);
}

public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Optional<Profile> findById(Long id);
    int deleteById(Long id);
}

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByArticleId(Long articleId);
    int deleteById(Long id);
}
```

#### Testing it out
Now that we’ve written our mutation resolvers, it’s time to test things out. Just like last time, run the application and go to http://localhost:8080/graphql to open GraphiQL. Now, let’s create a new article by creating the following mutation query:
```
mutation CreateArticle($input: CreateArticleInput!) {
  createArticle(input: $input) {
    id
    title
    author {
      id
      username
    }
  }
}
```
This looks quite similar to querying for data, with the only exception that we use the mutation keyword in stead of the query keyword. Additionally to this, we also have to create a query variable called input at the bottom of the GraphiQL interface. GraphiQL will automatically autocomplete certain stuff, so don’t be surprised!

```
{
  "input": {
    "title": "Writing GraphQL mutations with Spring boot",
    "text": "This is the text for our blog article.",
    "authorId": 1
  }
}
```
When executing this mutation query, you’ll see that the output becomes:

```
{
  "data": {
    "createArticle": {
      "id": 3,
      "title": "Writing GraphQL mutations with Spring boot",
      "author": {
        "id": 1,
        "username": "g00glen00b"
      }
    }
  }
}
```
![image info](./assets/Screenshot-2018-02-17-15.51.02.png "Screenshot of GraphiQL executing a mutation query")

To verify that this actually worked, we can use the query from last time to retrieve all queries.

![image info](./assets/Screenshot-2018-02-17-15.52.49.png "Screenshot of GraphiQL executing a query to retrieve all articles")

As you can see in the screenshot above, things are working quite nicely!

So, let’s clean things up now by deleting the article again by executing the following query:

```
mutation DeleteArticle($id: Int!) {
  deleteArticle(id: $id)
}
```
Just like before, we have to pass the ID of the article as a query parameter. In this case, the ID of the new article is 3:

```
{"id": 3}
```

When you execute this, you’ll see that you get a “1” as a result, because there was exactly one record deleted:

![image info](./assets/Screenshot-2018-02-17-15.55.51.png "Screenshot of GraphiQL executing a delete mutation")

However, when you execute it again, you’ll see that the result changes to zero, because there was no longer any record with ID 3. This indicates that our delete query was properly executed.

#### Error handling
As you can see in our code, when we try to update an article or profile that doesn’t exist, we throw either an ArticleNotFoundException or a ProfileNotFoundException. However, if we try this right now, we see we an internal server error:

![image info](./assets/Screenshot-2018-02-17-16.04.55.png "Screenshot of server error when updating a non-existing entity within GraphiQL")

This is not really what we want, considering that the exception is really on the users end, and not on the server. First of all, let’s alter the ArticleNotFoundException to implement the GraphQLError interface:

```
public class ArticleNotFoundException extends RuntimeException implements GraphQLError {
    private Long articleId;

    public ArticleNotFoundException(Long articleId) {
        this.articleId = articleId;
    }

    @Override
    public String getMessage() {
        return "Article with ID " + articleId + " could not be found";
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.ValidationError;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return Collections.singletonMap("articleId", articleId);
    }
}
```

GraphQLError indicates that this is a GraphQL related error. However, this is not enough. When you throw an exception, the default GraphQL resolver will automatically wrap that exception in a new GraphQLError called ExceptionWhileDataFetching. To solve this, we could create our own GraphQLErrorHandler implementation that unwraps the exception again, for example:

```
@Component
public class GraphQLErrorHandler implements graphql.servlet.GraphQLErrorHandler {

    @Override
    public List<GraphQLError> processErrors(List<GraphQLError> list) {
       return list.stream().map(this::getNested).collect(Collectors.toList());
    }

    private GraphQLError getNested(GraphQLError error) {
        if (error instanceof ExceptionWhileDataFetching) {
            ExceptionWhileDataFetching exceptionError = (ExceptionWhileDataFetching) error;
            if (exceptionError.getException() instanceof GraphQLError) {
                return (GraphQLError) exceptionError.getException();
            }
        }
        return error;
    }

}
```
This GraphQL error handler will check if the error is of type ExceptionWhileDataFetching and then if the exception itself is of type GraphQLError. If that’s the case, it will return the exception itself, otherwise it will return the error.

If we do the same thing for ProfileNotFoundException and run the application again. We can see that the result looks a lot better now:

![image info](./assets/Screenshot-2018-02-17-17.15.44-768x280.png "Screenshot of a custom GraphQL error in GraphiQL")

The only thing you may want to change now is to exclude the stacktrace itself, because that’s not interesting to expose to others. To do this, we can use the @JsonIgnore annotation:

```
@Override
@JsonIgnore
public StackTraceElement[] getStackTrace() {
    return super.getStackTrace();
}
```
With this, we now have a proper API for querying and mutating data using GraphQL!

All the code so far has been tagged as "graphql-with-schema". The next changes will include the SPQR implementation as well as contaienerizing the app via docker and adding monitoring to the application.