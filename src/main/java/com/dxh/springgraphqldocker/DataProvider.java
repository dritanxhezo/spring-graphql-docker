package com.dxh.springgraphqldocker;

import com.dxh.springgraphqldocker.model.Article;
import com.dxh.springgraphqldocker.model.Comment;
import com.dxh.springgraphqldocker.model.Profile;
import com.dxh.springgraphqldocker.repository.ArticleRepository;
import com.dxh.springgraphqldocker.repository.CommentRepository;
import com.dxh.springgraphqldocker.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@AllArgsConstructor
public class DataProvider implements CommandLineRunner {
    private CommentRepository commentRepository;
    private ArticleRepository articleRepository;
    private ProfileRepository profileRepository;

    @Override
    @Transactional
    public void run(String... strings) {
        Profile author = profileRepository.save(new Profile(null, "dxhezo", "The author of this blog"));
        Profile admin = profileRepository.save(new Profile(null, "admin", "The administrator of this blog"));
        Article article1 = articleRepository.save(new Article(null, "Hello wold", author.getId(), "This is a hello world"));
        Article article2 = articleRepository.save(new Article(null, "Foo", admin.getId(), "Bar"));
        commentRepository.save(new Comment(null, "Do you like this article?", article1.getId(), author.getId()));
        commentRepository.save(new Comment(null, "This is a great article", article1.getId(), admin.getId()));
        commentRepository.save(new Comment(null, "This is a comment", article2.getId(), admin.getId()));
    }
}