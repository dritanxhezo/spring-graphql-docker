package com.dxh.springgraphqldocker.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.dxh.springgraphqldocker.model.Article;
import com.dxh.springgraphqldocker.model.Comment;
import com.dxh.springgraphqldocker.model.Profile;
import com.dxh.springgraphqldocker.repository.ArticleRepository;
import com.dxh.springgraphqldocker.repository.CommentRepository;
import com.dxh.springgraphqldocker.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {
    private ArticleRepository articleRepository;
    private CommentRepository commentRepository;
    private ProfileRepository profileRepository;

    public List<Article> getArticles() {
        return articleRepository.findAll();
    }

    public List<Comment> getComments() {
        return commentRepository.findAll();
    }

    public List<Profile> getProfiles() {
        return profileRepository.findAll();
    }

    public Article getArticle(Long id) {
        return articleRepository.findById(id)
                                .orElse(new Article());
    }
}
