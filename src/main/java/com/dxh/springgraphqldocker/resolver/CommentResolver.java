package com.dxh.springgraphqldocker.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.dxh.springgraphqldocker.model.Comment;
import com.dxh.springgraphqldocker.model.Profile;
import com.dxh.springgraphqldocker.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CommentResolver implements GraphQLResolver<Comment> {
    private ProfileRepository profileRepository;

    public Profile getAuthor(Comment comment) {
        return profileRepository.findById(comment.getAuthorId())
                                .orElse(new Profile());
    }
}