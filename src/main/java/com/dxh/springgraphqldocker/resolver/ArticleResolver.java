package com.dxh.springgraphqldocker.resolver;


import com.coxautodev.graphql.tools.GraphQLResolver;
import com.dxh.springgraphqldocker.model.Article;
import com.dxh.springgraphqldocker.model.Profile;
import com.dxh.springgraphqldocker.model.Comment;
import com.dxh.springgraphqldocker.repository.CommentRepository;
import com.dxh.springgraphqldocker.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class ArticleResolver implements GraphQLResolver<Article> {
    private CommentRepository commentRepository;
    private ProfileRepository profileRepository;

    public Profile getAuthor(Article article) {
        return profileRepository.findById(article.getAuthorId())
                                .orElse(new Profile());
    }

    public List<Comment> getComments(Article article) {
        return commentRepository.findByArticleId(article.getId());
    }
}