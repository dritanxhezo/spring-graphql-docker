package com.dxh.springgraphqldocker.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dxh.springgraphqldocker.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List findByArticleId(Long articleId);
    void deleteById(Long id);
}