package com.dxh.springgraphqldocker.repository;

import com.dxh.springgraphqldocker.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    Optional<Article> findById(Long id);
    void deleteById(Long id);
}