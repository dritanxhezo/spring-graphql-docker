package com.dxh.springgraphqldocker.repository;

import com.dxh.springgraphqldocker.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Optional<Profile> findById(Long id);
    void deleteById(Long id);
}