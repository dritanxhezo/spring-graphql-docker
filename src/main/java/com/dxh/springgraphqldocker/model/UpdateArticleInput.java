package com.dxh.springgraphqldocker.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateArticleInput {
    private Long id;
    private String title;
    private String content;
}
